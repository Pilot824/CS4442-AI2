% Finding error rate for k = 1
[err_1, conf_1] = p2(p3(X_train, Y_train, X_test, 1), Y_test);
% Finding error rate for k = 3
[err_3, conf_3] = p2(p3(X_train, Y_train, X_test, 3), Y_test);
% Finding error rate for k = 5
[err_5, conf_5] = p2(p3(X_train, Y_train, X_test, 5), Y_test);
% Finding error rate for k = 7
[err_7, conf_7] = p2(p3(X_train, Y_train, X_test, 7), Y_test);

disp(['Error Rate (k=1): ', num2str(err_1)])
disp(['Error Rate (k=3): ', num2str(err_3)])
disp(['Error Rate (k=5): ', num2str(err_5)])
disp(['Error Rate (k=7): ', num2str(err_7)])