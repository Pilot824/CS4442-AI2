clc;
clear;

disp('Loading Data...')
load A1;
disp('Done')

disp('Generating Test Data - p1....')
[X_out, Y_out] = p1(X_train, Y_train, 5, 10);
disp('Done')

disp('Calculating Weights - p5 (100 iterations)...')
w_100 = p5(X_out, Y_out, 100);
disp('Done')

disp('Calculating Weights - p5 (1000 iterations)...')
w_1000 = p5(X_out, Y_out, 1000);
disp('Done')

disp('Calculating Weights - p5 (10000 iterations)...')
w_10000 = p5(X_out, Y_out, 10000);
disp('Done')

disp('Calculating Error Rate for 100 iterations...')
C_100 						= p4(w_100, X_train);
[test_err_100, conf_100] 	= p2(C_100, Y_train);
disp('Done')

disp('Calculating Error Rate for 100 iterations...')
C_1000 						= p4(w_1000, X_train);
[test_err_1000, conf_100] 	= p2(C_1000, Y_train);
disp('Done')

disp('Calculating Error Rate for 100 iterations...')
C_10000 					= p4(w_10000, X_train);
[test_err_10000, conf_100] 	= p2(C_10000, Y_train);
disp('Done')

disp(['Error Rate (100): ', num2str(test_err_100)])
disp(['Error Rate (1000): ', num2str(test_err_1000)])
disp(['Error Rate (10000): ', num2str(test_err_10000)])