% p6
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

function w = p6(X_Train, Y_Train, iterNum, wInit, alpha )

	% Initiating weight matrix
	w = wInit;
	last_loss = Inf;

	% Run logistic regression batch rule
	for i = 1:iterNum

		% Computing y(az) for all samples
		calc = Y_Train .* (X_Train * w);

		loss = abs(sum(calc(find(calc < 0))));

		w = sum( [transpose(alpha * (Y_Train(find(calc<0)) .* X_Train(find(calc<0), :))), w], 2);

		if loss > last_loss
			disp( ['loss > last_loss = worse'] )
			%alpha = alpha / 2;
		end
		
		last_loss = loss;

	end



end