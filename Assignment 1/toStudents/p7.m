% p7
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

% Determines classification, C, of entries from X given W weights

function C = p7(W, X)

	[x_rows, x_cols] = size(X);

	% Adding bias and X*tranpose(W) to other cols;

	X = [ones(x_rows, 1), X];

	mult = X * transpose(W);

	% x_calc = [ones(x_rows, 1), X*transpose(W) ];

	[M, C] = max(transpose(mult));

	C = transpose(C);

end