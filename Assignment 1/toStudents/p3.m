% p3
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca


function C = p3(X_train, Y_train, X_test, k)

	clc;

	C = 0;
    
    % Number of training entries
    numb_trg_entries = length(X_train);

    %Normalizing all entries in X_train
	%C_normal = (X_train-repmat(mean(X_train), numb_trg_entries, 1))*diag(1./std(X_train));
	% Dont need to normalize

	% For each entry
	for entry_i = 1 : length(X_test)

		testMatrix = repmat( X_test(entry_i,:), numb_trg_entries, 1 );

		absDiff = abs( X_train - testMatrix );

		absDiff = absDiff.^2;

		dist = sum( absDiff, 2 );

		[Y, I] = sort(dist);

		neighborsInd = I(1:k);

		neighbors = Y_train(neighborsInd);

		C(entry_i, 1) = mode(neighbors);

	end

end

