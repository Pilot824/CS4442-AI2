% softmax_calc
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

% Calculates softmax function for array

function sm = softmax_calc(array)
	denom = 0;

	sm = zeros(length(array), 1);

	for i = 1 : length(array)
		denom = denom + exp(array(i));
	end

	for i = 1 : length(array)
		sm(i) = exp(array(i)) / denom;
	end
end