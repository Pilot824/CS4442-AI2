% p1
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

function [X_out, Y_out] = p1(X, Y, l1, l2)

    %list of entries based on L1 and L2
    l1_indexes = find(Y == l1);
    l2_indexes = find(Y == l2);
    
    Y_out_1 = repmat([1], length(l1_indexes), 1);
    Y_out_2 = repmat([2], length(l2_indexes), 1);
    Y_out = [Y_out_1; Y_out_2];
    
    X_out_1 = X(l1_indexes, :);
    X_out_2 = X(l2_indexes, :);
    
    X_out = [X_out_1; X_out_2];

end