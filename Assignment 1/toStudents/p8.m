% p8
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

function w = p8(X_Train, Y_Train, iterNum, wInit, alpha )

	% Initiating weight matrix
	w = wInit;
	last_loss = Inf;

	[x_rows, x_cols] = size(X_Train);

	% Run logistic regression batch rule
	for i = 1:iterNum

		% Adding in bias to X_Train
		X_Train = [ones(x_rows, 1), X_Train ];

		% Multiplying X_Train and weights
		wx = (X_Train * transpose(w));

		% Finding max in all entries
		[M, I] = max( transpose(wx) );

		% Single Sample Update Rule: W = W − ( Wx − y ) x^t

		for w_i = 1 : length(w)

			w(w_i) = w(w_i) - alpha*( wx(w_i) - Y_Train(w_i) ) * transpose(X_Train(w_i));

		end

	end


end